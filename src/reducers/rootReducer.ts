import { combineReducers } from 'redux';
import pinLocationReducer from '../components/mapView/reducer';

// combine all reducers
export const rootReducer = combineReducers({
  googleMapView: pinLocationReducer,
});
