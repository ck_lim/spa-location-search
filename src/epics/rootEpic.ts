import { combineEpics } from 'redux-observable';
import pinLocationEpic from '../components/mapView/epic';

// combine all epics
export default combineEpics(pinLocationEpic);
