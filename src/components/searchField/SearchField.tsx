import './searchField.css';
import { useEffect, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import { Input } from 'antd';
import { pinLocation } from '../mapView/action';

export const SearchField = () => {
  const dispatch = useDispatch();
  const autoCompleteRef = useRef<google.maps.places.Autocomplete | null>(null);
  const inputRef = useRef<HTMLDivElement>(null);

  const options = {
    fields: ['geometry', 'name'],
    types: [],
  };

  const [keyword, setKeyword] = useState('');

  useEffect(() => {
    // set up Places Autocomplete and bint to Input textfield
    autoCompleteRef.current = new google.maps.places.Autocomplete(
      inputRef.current?.children[0] as HTMLInputElement,
      options,
    );
    autoCompleteRef.current.addListener('place_changed', function () {
      const place = autoCompleteRef.current?.getPlace();
      if (place != null) {
        setKeyword(place.name ?? '');
        dispatch(pinLocation(place));
      }
    });
  }, []);

  return (
    <div ref={inputRef}>
      <Input
        className='text-field'
        placeholder='Search here...'
        value={keyword}
        onChange={(event) => setKeyword(event.target.value)}
        style={{ width: 350 }}
      />
    </div>
  );
};
export default SearchField;
