import './loader.css';
import Spin from 'antd/es/spin';

export const Loader = () => {
  return (
    <div className='center-aligned'>
      <Spin />
    </div>
  );
};
