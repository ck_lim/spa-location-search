import { PIN_LOCATION, PIN_LOCATION_SUCCESS } from '../action';

const history: google.maps.places.PlaceResult[] = [];
const initialState = { history: [] };

export function pinLocationReducer(state = initialState, action: any) {
  switch (action.type) {
    case PIN_LOCATION: {
      const result = action.payload;
      history.push(result);
      return {
        ...state,
        state: PIN_LOCATION_SUCCESS,
        data: result,
        history: history,
      };
    }
    default:
      return state;
  }
}

// selectors
export const selectPinnedLocation = (state: any) => {
  if (state['googleMapView']['state'] == PIN_LOCATION_SUCCESS) {
    if (state['googleMapView']['data'] != null) {
      const location = state['googleMapView']['data']['geometry']['location'];
      return location;
    }
  }
  // default coordinate to Kuala Lumpur
  return {
    lat: 3.140853,
    lng: 101.693207,
  };
};
export const selectSearchHistory = (state: any) => {
  if (state['googleMapView']['state'] == PIN_LOCATION_SUCCESS) {
    const dataList: any[] = [];
    history.forEach((x, index) => {
      dataList.push({
        key: index,
        name: x.name ?? '',
        place: x,
      });
    });
    return dataList.reverse(); // sort latest search on top
  }
  return [];
};
