import { GoogleMap, MarkerF } from '@react-google-maps/api';
import './googleMapView.css';
import { useSelector } from 'react-redux';
import { selectPinnedLocation } from './reducer/googleMapViewReducer';

export const GoogleMapView = () => {
  const pinnedLocation = useSelector(selectPinnedLocation);

  return (
    <GoogleMap
      mapContainerClassName='map-view-container'
      options={{
        mapTypeControl: false, // hide map type : Map / Satellite
        fullscreenControl: false, // hide full screen mode
        streetViewControl: false, // hide street view
      }}
      center={pinnedLocation}
      zoom={15}
    >
      <MarkerF position={pinnedLocation} />
    </GoogleMap>
  );
};
