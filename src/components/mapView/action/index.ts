export const PIN_LOCATION = 'PIN_LOCATION';
export const PIN_LOCATION_SUCCESS = 'PIN_LOCATION_SUCCESS';

export function pinLocation(place: google.maps.places.PlaceResult) {
  return {
    type: PIN_LOCATION,
    payload: place,
  };
}

export function pinLocationSuccess(place: google.maps.places.PlaceResult) {
  return {
    type: PIN_LOCATION_SUCCESS,
    payload: place,
  };
}
