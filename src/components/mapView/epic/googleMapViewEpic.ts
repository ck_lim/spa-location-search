import { mergeMap } from 'rxjs';
import { ofType } from 'redux-observable';
import { PIN_LOCATION, pinLocationSuccess } from '../action';

export const pinLocationEpic = (action: any) =>
  action.pipe(
    ofType(PIN_LOCATION),
    mergeMap(async () =>
      pinLocationSuccess(action.payload as google.maps.places.PlaceResult),
    ),
  );
