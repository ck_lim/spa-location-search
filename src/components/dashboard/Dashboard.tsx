import HistoryTable from '../history/HistoryTable';
import { GoogleMapView } from '../mapView/GoogleMapView';
import { SearchField } from '../searchField/SearchField';
import './dashboard.css';

export const Dashboard = () => {
  return (
    <div>
      <SearchField />
      <HistoryTable />
      <GoogleMapView />
    </div>
  );
};
