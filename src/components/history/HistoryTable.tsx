import { Table } from 'antd';
import './historyTable.css';
import { ColumnsType } from 'antd/es/table/interface';
import { useDispatch, useSelector } from 'react-redux';
import { selectSearchHistory } from '../mapView/reducer/googleMapViewReducer';
import { pinLocation } from '../mapView/action';

interface DataType {
  name: string;
  place: google.maps.places.PlaceResult;
}

export const HistoryTable = () => {
  const dispatch = useDispatch();
  const historyData = useSelector(selectSearchHistory);

  const columns: ColumnsType<DataType> = [
    {
      title: 'History',
      dataIndex: 'name',
      key: 'name',
      render: (text) => <a>{text}</a>,
    },
  ];

  return (
    <Table
      className='tb-history'
      columns={columns}
      dataSource={historyData}
      pagination={{ pageSize: 5 }} // set max row number in 1 page
      onRow={(data) => {
        return {
          onClick: () => dispatch(pinLocation(data['place'])), // go to location that is searched before from history table
        };
      }}
    />
  );
};

export default HistoryTable;
