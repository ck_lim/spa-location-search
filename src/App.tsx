import './App.css';
import { useLoadScript } from '@react-google-maps/api';
import Loader from './components/loader';
import { Dashboard } from './components/dashboard/Dashboard';

function App() {
  // load google map api key
  const { isLoaded } = useLoadScript({
    googleMapsApiKey: process.env.REACT_APP_GOOGLE_API_KEY || '',
    libraries: ['drawing', 'geometry', 'places'],
  });

  return <div className='App'>{!isLoaded ? <Loader /> : <Dashboard />}</div>;
}

export default App;
