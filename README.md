# Introduction

This is a ReactJS project that allows user to search location on Google Map.

## Getting Started

You can clone the repository and run the commands below to install the necessary tools and run the application.

```cmd
npm install
npm start
```

Do note that you must create .env file and place your Google API key in the .env file

```cmd
REACT_APP_GOOGLE_API_KEY=<GOOGLE API KEY HERE>
```

## Dev Libraries / Components

This project relies on the following important libraries to speed up the development process:

- [Place Autocomplete](https://developers.google.com/maps/documentation/javascript/examples/places-autocomplete)
- [Ant Design](https://ant.design/components/overview)
- [typescript](https://www.npmjs.com/package/typescript)
- [react-redux](https://www.npmjs.com/package/react-redux)
- [redux-observable](https://www.npmjs.com/package/redux-observable)
- [rxjs](https://www.npmjs.com/package/rxjs)
